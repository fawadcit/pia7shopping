//
//  ViewController.swift
//  pia7 shopping
//
//  Created by Fawad Sadiqmal on 10/19/17.
//  Copyright © 2017 Fawad Sadiqmal. All rights reserved.
//

import UIKit
import FirebaseDatabase

class ViewController: UIViewController {
    
    @IBOutlet weak var fruitTextfield: UITextField!
    
    var ref: DatabaseReference!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
       ref = Database.database().reference()
        
  
        //HÄMTA DATA
        ref.child("pia7").child("morestuff").observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? NSDictionary
            let fruittexten = value?["fruit"] as? String ?? ""
            
            print("FRUKTEN ÄR "+fruittexten)
            
            self.fruitTextfield.text = fruittexten
            
        }) { (error) in
            print(error.localizedDescription)
        }
       
     
        func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

 }
    
    @IBAction func saveTheFruit(_ sender: Any) {
        //SPARA DATA
        ref.child("pia7").child("morestuff").setValue(["fruit": fruitTextfield.text])
    }
    
}

